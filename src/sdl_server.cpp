/* $Id$ $URL$ */
#include "sdl_server.h"

/**\file sdl_server.cpp
 * Definition of the Listener class and SDL Server framework
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision: 7.3 $
 * $Date$
 */

//#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <functional>
#include <errno.h>

#include <stdexcept>
#include <sstream>
#include <SDL_version.h>

enum {INITCODE = 0xABCDEF98};
//static unsigned long initted = 0;

#ifdef __WIN32__
#include <winsock.h>
#else
#include <sys/socket.h>
#endif
extern "C" {
    struct Guessed_TCPsocket {
        int ready;
        int channel;
    };

    /* Shutdown a TCP network socket */
    void SDLNet_TCP_Shutdown_EXT(TCPsocket xsock) {
        Guessed_TCPsocket *sock = (Guessed_TCPsocket*)(xsock);
	if ( sock != NULL ) {
		if ( sock->channel != -1 ) {
			/* 2nd arg is SHUT_RDWR on POSIX, RD_BOTH on win32 == 0x02 on both (by definition) */
			shutdown(sock->channel, 2);
		}
		/* at this point we are still valid, just not in a
		 * mode that sends or receives data */
        }
    }
}

bool Listener::enabled = false;
Listener::FACTORY_VEC Listener::factories; //(init_factory());

bool Listener::addServer(Server::CLIENT_FACTORY factory, Uint16 port) {
    factories.push_back(new Server(factory, port));
    fprintf(stderr, "Added a server factory in Factory(%p) on port %hu; now %u.\n",
            (void*)&factories, port, (unsigned)factories.size());
    return true;
}


int Listener::init() {
    //if (!RConfig::SOCKET_ENABLED)
    //    return 1;
    if (NetUtil::init() == -1) {
        fprintf(stderr, "No Network: SDLNet_Init: %s\n", SDLNet_GetError());
        return -1;
    }
    enabled = true;
    return 0;
}

void Listener::quit() {

    NetUtil::quit();
    enabled = false;

}

Listener::Listener(const Listener&) {} //private

Listener::Listener(Environment *eenv, unsigned timeout_ms)
    :
env(eenv), servers(0), servers_ready(0), thr(0), ready(false), clientMut(SDL_CreateMutex()), timeout(timeout_ms), resolvehost(true) {
    if (!Listener::enabled)
        return;

    servers = SDLNet_AllocSocketSet(factories.size());

    if (!servers) {
        fprintf(stderr, "SDLNet_AllocSocketSet(%u): %s\n",
                (unsigned)factories.size(), SDLNet_GetError());
        return;
    }
    fprintf(stderr, "Trying to create %u servers from factories in Factory(%p)\n",
            (unsigned)factories.size(), (void*)&factories);
    for (FACTORY_VEC::iterator it = factories.begin(); it != factories.end(); ++it) {
        if ((*it)->open()) {
            if (SDLNet_TCP_AddSocket(servers, (*it)->socket) < 0) {
                fprintf(stderr, "SDLNet_TCP_AddSocket: %s\n", SDLNet_GetError());
                return;
            } else {
                servers_ready++;
            }
        }
    }
    ready = true;
}

Listener::~Listener() {
    if (servers)
        SDLNet_FreeSocketSet(servers);
    SDL_DestroyMutex(clientMut);
}


int Listener::listenLoaderThread(void *listener) {
    Listener *l = (Listener*)listener;
    return l->listen();
}

void Listener::start() {
#if SDL_VERSION_ATLEAST(2, 0, 0)
    thr = SDL_CreateThread(&Listener::listenLoaderThread, "sdlserver_listener", this);
#else
    thr = SDL_CreateThread(&Listener::listenLoaderThread, this);
#endif
}

void Listener::stop() {
    //LOCK!
    ready = false;
    const FACTORY_VEC::iterator e = factories.end();
    for (FACTORY_VEC::iterator it = factories.begin(); it != e; ++it) {
        // the first one of these should wake up the ::listen thread
        // ready is false, so it should exit straight away
        // but we're not ready yet to Close, until we know listen has joined
        (*it)->shutdown();
    }
    stopClients();
}

int Listener::join() {
    FACTORY_VEC v(factories);
    factories.clear();
    int status;
    joinClients();
    SDL_WaitThread(thr, &status);

    const FACTORY_VEC::iterator e = v.end();
    for (FACTORY_VEC::iterator it = v.begin(); it != e; ++it) {
        delete *it;
    }
    return status;
}

int Listener::listen() {
    TCPsocket peer;
    fprintf(stderr, "Checking for new socket connections on %d sockets. T/O = %dms...\n", servers_ready, timeout);
    while (ready) {
        //errno = 0;
        int numready = SDLNet_CheckSockets(servers, timeout);
        if (!ready)
            break;
        if (numready < 0) {
            fprintf(stderr, "SDLNet_CheckSockets: %s\n", SDLNet_GetError());
            //perror("SDLNet_CheckSockets perror");
            //fprintf(stderr, "errno = %d\n", errno);
//            if ( errno != EINTR &&  //interrupted system calls are usually harmless
//                 errno != EAGAIN) { //resource temporarily unavailable
                ready = false;
                return 1;
//            } else {
//                SDL_Delay(200); //wait 200ms so we don't thrash CPU for real errors
//            }
        } else if (numready > (int)(factories.size())) {
            fprintf(stderr, "More than %u sockets ready -- shouldn't happen. Killing net.\n",
                    (unsigned)factories.size());
            ready = false;
            return 2;
        } else if (numready > 0) {
            for (FACTORY_VEC::iterator it = factories.begin(); it != factories.end(); ++it) {
                if (SDLNet_SocketReady((*it)->socket)) {
                    peer = SDLNet_TCP_Accept((*it)->socket);
                    if (!peer) {
                        fprintf(stderr, "SDLNet_TCP_Accept: %s\n", SDLNet_GetError());
                    } else {
                        Client *cl = (*it)->factory(peer, this);

                        IPaddress *addr = SDLNet_TCP_GetPeerAddress(peer);

                        cl->peerdotted = NetUtil::addr2ipstr(addr);

                        /* NOT THREAD SAFE!! */
                        const char* hostname = resolvehost ? SDLNet_ResolveIP(addr) : "";

                        cl->peerhost = hostname ? hostname : cl->peerdotted;

                        //fprintf(stderr, "Connection from %s - %s:%u on port %u\n",
                        //        hostname, cl->peerdotted.c_str(),
                        //        (unsigned)addr->port, (unsigned)((*it)->port)
                        //       );
                        SDL_mutexP(clientMut);
                        clients.push_back(cl);
                        SDL_mutexV(clientMut);
                        cl->serve();
                    }
                }
            }
        } else {
            /* timeout */
        }
    }
    fprintf(stderr, "Listener::listen() exiting [ready = %s]\n", ready ? "true" : "false");
    return 0;
}

void Listener::clientFinished(Client *cl) {
    // Don't proceed if we are waiting for all clients to stop;
    // otherwise it would cause a deadlock.
    if (stoppingClients)
        return;

    SDL_mutexP(clientMut);
    CLIENT_VEC::iterator it = std::find(clients.begin(), clients.end(), cl);
    if (it != clients.end()) {
        clients.erase(it);
        delete cl;
    }
    SDL_mutexV(clientMut);
}

const Listener::CLIENT_VEC& Listener::getClients() const {
    return clients;
}

void Listener::stopClients() {
    SDL_mutexP(clientMut);
    stoppingClients = true;
    std::for_each(clients.begin(), clients.end(), std::mem_fun(&Client::stop));
    stoppingClients = false;
    SDL_mutexV(clientMut);
}

void Listener::joinClients() {
    SDL_mutexP(clientMut);
    stoppingClients = true;
    std::for_each(clients.begin(), clients.end(), std::mem_fun(&Client::join));
    stoppingClients = false;
    SDL_mutexV(clientMut);
}

Server::~Server () {
    if (socket) {
        fprintf(stderr, "Closing server socket %p\n", (void*)this);
        SDLNet_TCP_Close(socket);
    }
}

void Server::shutdown () {
    if (socket) {
        fprintf(stderr, "Shutting down server socket %p\n", (void*)this);
        SDLNet_TCP_Shutdown_EXT(socket);
    }
}

bool Server::open() {
    IPaddress ip;
    fprintf(stderr, "Creating a Server on port %hd... ", port);
    if (SDLNet_ResolveHost(&ip, NULL, port) != 0) {
        fprintf(stderr, "\nError: SDLNet_ResolveHost: %s\n", SDLNet_GetError());
        return false;
    }
    socket = SDLNet_TCP_Open(&ip);
    if (!socket) {
        fprintf(stderr, "\nError: SDLNet_TCP_Open (%d:%hd): %s\n", ip.host, ip.port, SDLNet_GetError());
        return false;
    }
    fprintf(stderr, "Listening on port %d\n", port);
    return true;
}

Client::Client() : s(0), creator(0), thr(0), run(0) {}
Client::Client(const Client&) : s(0), creator(0), thr(0), run(0) {}

#ifdef _MSC_VER
// MSVC does not allow dllimporting a class with an undefined method.
Client& Client::operator=(const Client&) {
    throw std::logic_error("Client::operator= disabled");
}
#endif

/** The function passed to the thread creator for Client handlers */
int Client::clientLoaderThread(void *client) {
    Client *c = (Client*)client;
    return c->doHandle();
}

void Client::serve() {
#if SDL_VERSION_ATLEAST(2, 0, 0)
    thr = SDL_CreateThread(&Client::clientLoaderThread, "sdlserver_client_serve", this);
#else
    thr = SDL_CreateThread(&Client::clientLoaderThread, this);
#endif
}

void Client::stop() {
    run = false;
    //just call close on the socket, even though it is still listening
    //SDLNet_TCP_Close(s);
    SDLNet_TCP_Shutdown_EXT(s);
    fprintf(stderr, "Closed socket %p running on thread %p\n", (void*)s, (void*)thr);
}
int Client::join() {
    int status;
    fprintf(stderr, "Waiting for thread %p to finish in Client::join()\n", (void*)thr);
    if (thr)
        SDL_WaitThread(thr, &status);
    if (s) {
        SDLNet_TCP_Close(s);
        s = 0;
    }
    fprintf(stderr, "Thread exited with status %d in Client::join()\n", status);
    return status;
}

int Client::doHandle() {
    run = true;
    int rv = 0;
    try {
        rv = handle();
    } catch (const std::exception& e){
        auto blah = e.what();
        fprintf(stderr, e.what());
    } catch (...) {
        fprintf(stderr, "Unknown exception caught in client\n");
    }
#ifndef NDEBUG
    IPaddress *addr = SDLNet_TCP_GetPeerAddress(s);
    if (addr) {
    /* NOT THREAD SAFE!! */
        //fprintf(stderr, "Connection from %s closed\n", NetUtil::addr2ipstr(addr, true).c_str());
    }
#endif
    SDLNet_TCP_Close(s);
    s = 0;
    run = false;
    creator->clientFinished(this);
    return rv;
}

Client::Client (TCPsocket ss, Listener *ccreator)
:
s(ss), creator(ccreator), thr(0), run(false) {
}

Client::~Client() {

}
namespace {
    bool dopause() {
        fprintf(stderr, "SDLNet_TCP_Recv returned EAGAIN -- IT SHOULDN'T -- there is a bug in it. Waiting 100ms.\n");
        SDL_Delay(100);
        return true;
    }
}

int NetUtil::init() {
    return SDLNet_Init();
}

void NetUtil::quit() {
    SDLNet_Quit();
}

bool NetUtil::mustRead(TCPsocket s, Uint8 **data, Uint32 size, bool allocate, volatile bool *run) {
    Uint32 offset = 0;
    int result = 0;
    bool dummyrun = true;
    if (!run) {
        run = &dummyrun;
    }
    if (allocate) {
        if (!(*data = new (std::nothrow) Uint8[size])) {
            fprintf(stderr, "Couldn't allocate %u bytes for Client::must_read\n", size);
            return false;
        }
    }

    Uint8* buf = *data;
    memset(buf, 0xff, size);
    do {
        errno = 0;
        while (*run && (result = SDLNet_TCP_Recv(s, buf+offset, size-offset)) > 0 && *run && result+offset < size) {
            offset += result;
        }
    } while (errno == EAGAIN && dopause());
    //fprintf(stderr, "Read %u + %d = %d bytes\n", offset, result, offset+result);
    if (!*run || result <= 0) {
        if (allocate)
            delete[] buf;
        if (*run) {
            fprintf(stderr, "Socket read error at %u/%u bytes in Client::must_read:\n", offset, size);
            fprintf(stderr, "\t%s, %s\n", SDLNet_GetError(), SDL_GetError());
            fprintf(stderr, "\terrno = %d, strerror(errno) = %s\n", errno, strerror(errno));
        }
        return false;
    }
    return true;
}

const char* BufferedLineReader::fillBuffer() {
    if (pos != 0) {
        //put the remains in the other buffer and switch
        memcpy(buffer[(active+1)&1], buffer[active] + pos, end-pos);
        active = (active+1)&1;
        pos = 0;
        end -= pos;
    }
    //here pos == 0, always

    //now fill up the rest of the buffer
    int result = SDLNet_TCP_Recv(s, buffer[active], bsz-end);
    if (result <= 0)
        return 0; //error occurred

    end += result;

    const char* hl = haveLine();
    if (hl)
        return hl;

    if (end == bsz) {
        pos = bsz;
        return (const char*)buffer[active]; //the buffer is not big enough for a whole line
    }

    //if we get here, then Recv did not read all of what we asked
    //so what do we do?
    // a) try to read again (might block)
    // b) just put a \0 on the end and return what we have
    //we choose (b)

    buffer[active][end] = 0;
    return (const char*)buffer[active];
}

namespace {
    const std::string& readError(TCPsocket s, int result) {
	fprintf(stderr, "[SS] Read error from socket %p, result was %d\n", (void*)s,result);
        fprintf(stderr, "[SS] SDLNet_GetError() = %s\n", SDLNet_GetError());
	return NetUtil::READ_ERROR;
    }
}

std::string NetUtil::readLine(TCPsocket s) {
    std::string str;
    char c;
    int res;
    if ((res = SDLNet_TCP_Recv(s, &c, 1)) != 1)
	return readError(s, res);
    while (c != '\n') {
        if (c != '\r')
            str += c;
	if ((res = SDLNet_TCP_Recv(s, &c, 1)) != 1)
	    return readError(s, res);
    }
    return str;
}

namespace NetUtil {

    std::string addr2ipstr(IPaddress* ip, bool doport) {
        std::ostringstream oss;
        // IPaddress host and port are always big endian, so convert to native format first.
        Uint32 addr = SDLNet_Read32(&ip->host);
        oss << ((addr>>24)&0xff) << '.' << ((addr>>16)&0xff) << '.' << ((addr>>8)&0xff) << '.' << (addr &0xff);
        if (doport)
            oss << ':' << SDLNet_Read16(&ip->port);
        return oss.str();
    }

    std::string host2ipstr(const std::string& hostname, long port) {
        IPaddress ip;
        if (SDLNet_ResolveHost(&ip, hostname.c_str(), port >= 0 ? port : 0) != 0) {
            fprintf(stderr, "\nError: SDLNet_ResolveHost: %s\n", SDLNet_GetError());
            return "";
        } else {
            return addr2ipstr(&ip);
        }
    }

    std::string localIPAddr(long port) {
        return host2ipstr("127.0.0.1", port);
    }

    const std::string READ_ERROR = "";
}
