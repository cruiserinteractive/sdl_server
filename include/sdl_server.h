/* $Id$ $URL$ */
#ifndef SDL_SERVER_DOT_AITCH
#define SDL_SERVER_DOT_AITCH

/**\file sdl_server.h
 * Declarations for the Client, Server and Listener classes that accepts socket commands / data
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision: 7.2 $
 * $Date$
 */

#ifdef _MSC_VER
#ifdef SDL_SERVER_EXPORTING
#define SDL_SERVER_API __declspec(dllexport)
#else
#define SDL_SERVER_API __declspec(dllimport)
#endif
#else
#define SDL_SERVER_API //nothing
#endif

#if defined DEBUG_STL && defined __GNUC__ && __GNUC__ >= 4
#undef _GLIBCXX_VECTOR
#include <debug/vector>
#if __GNUC_MINOR__ >= 2
namespace stl = std::__debug;
#else
namespace stl = __gnu_debug_def;
#endif
#else
#include <vector>
namespace stl = std;
#endif

#include <string>
#include <assert.h>

#include <SDL.h>
#include <SDL_net.h>
#include <SDL_thread.h>

class Environment;
class Listener;

extern "C" {
    /* Shutdown a TCP network socket */
    void SDLNet_TCP_Shutdown_EXT(TCPsocket sock);
}

/** SDL_net utility functions */
namespace NetUtil {
    /**
     * The string that is returned by readLine for read errors
     */
    extern SDL_SERVER_API const std::string READ_ERROR;
    /**
     * Read a line until \\n or \\r (discard \\n and/or \\r)
     */
    SDL_SERVER_API std::string readLine(TCPsocket s);
    /**
     * This *must* read \a size bytes from the socket. It will only return
     * once \a size bytes have been put into \a *data, or the socket has been
     * \a shutdown/closed/broken.
     *
     * \param s The socket to read from
     * \param data a pointer to the buffer (which must be allocated if \a allocate is false)
     * \param size the number of bytes to read into \a *data
     * \param allocate if true, must_read will allocate \a size bytes and set \a *data AND
     *        we will free it if there is an error
     * \param run if non-null the run variable to monitor (?)
     *
     * \returns false only if the socket has closed and \a size bytes have not yet been read
     */
    SDL_SERVER_API bool mustRead(TCPsocket s, Uint8 **data, Uint32 size, bool allocate = true, volatile bool *run = 0);

    /**
     * Return a reliable dotted-decimal / coloned-hex address string for an address
     *
     * \param addr the address/port
     * \param doport if true, append the port number in the address
     */
    SDL_SERVER_API std::string addr2ipstr(IPaddress* addr, bool doport = false);

    /**
     * Resolve the hostname and return a reliable dotted/coloned IP-address identifier
     *
     * \param hostname the host to resolve
     * \param port if specified, append the port
     * \return "" on hostname resolution error
     */
    SDL_SERVER_API std::string host2ipstr(const std::string& hostname, long port = -1);

    /**
     * Return the dotted decimal IP address of the local host
     *
     * \param port if specified, append the port
     */
    SDL_SERVER_API std::string localIPAddr(long port = -1);

    /** Initialize SDL_Net */
    SDL_SERVER_API int init();

    /** Quit SDL_Net */
    SDL_SERVER_API void quit();
}

/**
 * Represents a client for the server to be handled in a separate thread
 */
class SDL_SERVER_API Client {
private:
    /** The thread function -- calls client->doHandle() */
    static int clientLoaderThread(void *client);

    /**
     * Handle a client (in a separate thread) --
     * calls (virtual) handle() after setting run variables
     */
    int doHandle(); //don't call this

protected:
    TCPsocket s;              ///< The socket of the client/peer
    Listener *creator;        ///< The Listener that created this Client
    SDL_Thread *thr;          ///< The thread that is handling the serving
    virtual int handle() = 0; ///< Override this for Client subclasses (but don't call it yourself)

    /** Read from the socket, returning false if it fails in any way -- see NetUtil::mustRead */
    bool must_read(Uint8 **data, Uint32 size, bool allocate = true) {return NetUtil::mustRead(s, data, size, allocate, &run);}

    /** Constructor does nothing, but is protected so only implementations can construct */
    Client();
    /** Copy constructor */
    Client(const Client&);
    Client& operator= (const Client&); ///< Assignment operator disabled (undefined)

public:
    std::string peerhost;     ///< Resolved hostname of the peer (or \a peerdotted if unavailable)
    std::string peerdotted; ///< Dotted-decimal representation of the peer, as a string
    volatile bool run;        ///< Set to false to cease execution (eventually)
    /** Create a new Client for the (connected) socket, \a ss */
    Client (TCPsocket ss, Listener *ccreator);
    /** Create and SDL Thread and start handling */
    void serve();
    /** Virtual Descrutctor -- does nothing */
    virtual ~Client();

    /** Tell the client thread that it should stop ASAP */
    virtual void stop();// {run = false;}
    /** Block until the client thread has finished */
    virtual int join();// {int status; SDL_WaitThread(thr, &status); return status;}
};


/** Encapsulation of a server */
class Server {
    Server& operator=(const Server& rhs);
    Server(const Server& r);
public:
    /** Type for client handlers */
    typedef Client * (*CLIENT_FACTORY)(TCPsocket, Listener*);
    CLIENT_FACTORY factory; ///< The client handler factory
    Uint16 port;            ///< The port we bind for listening
    TCPsocket socket;       ///< The server (listening) socket

    /** Create a Server for a particular handler on a particular port */
    Server (CLIENT_FACTORY factory_, Uint16 port_)
        :
    factory(factory_), port(port_), socket(0) {}

    /** Destructor -- releases the socket */
    ~Server ();

    /** Open the socket and wait for connections to accept() */
    bool open();

    /** Shutdown the server socket */
    void shutdown();
};

/**
 * The Server -- listens on [a number of sockets] for a connection
 * for which to start a Client
 */
class SDL_SERVER_API Listener {
    /** defaults are fine here */
    Listener(const Listener&);
    Listener& operator=(const Listener&) {return *this;}
private:
    /** The thread function -- calls listener->listen() */
    static int listenLoaderThread(void* listener);

    /** The server thread */
    int listen();
protected:
//    enum {NUM_SERVERS = 2 /**< How many ports are we listening on */};
    static bool enabled;  ///< Whether listening is enabled (if false, functions early-exit)
public:
    typedef stl::vector<Client*> CLIENT_VEC; ///< Client vector type
    Environment *env;     ///< The Environment for which we are listening (can be 0)
protected:

    //    TCPsocket image_sock;     ///< Server socket for image inputs
    //    TCPsocket command_sock;   ///< Server socket for other commands

//    /** The type for Client factories -- Client class makers */
//    typedef Client * (*CLIENT_FACTORY)(TCPsocket, Listener*);
//
//    TCPsocket server_socks[NUM_SERVERS];   ///< The server sockets (on which we listen)
//    Uint16 server_ports[NUM_SERVERS];      ///< The ports on which we are listening
//    CLIENT_FACTORY factories[NUM_SERVERS]; ///< The factories corresponding to each port

    SDLNet_SocketSet servers; ///< We need this to poll/timeout effectively in SDL
    int servers_ready;        ///< How many of the client handlers were initialised successfully
    SDL_Thread *thr;          ///< The listening thread
    volatile bool ready;      ///< Thread terminator variable
    volatile bool stoppingClients; ///< Whether this listener is in the process of stopping clients
    SDL_mutex *clientMut;      ///< Client Mutex
    CLIENT_VEC clients;       ///< Running clients
    unsigned timeout;         ///< The timeout for poll() (in milliseconds)
    bool resolvehost;        ///< Whether to resolve the hostname of the peer when connected
public:

    /** Initialize Listener and SDL_Net */
    static int init();

    /** Quit (deinitialize) Listener and SDL_Net */
    static void quit();

    /**
     * Create a Listener for \a env -- initialises factories and
     * opens up all the server sockets
     */
    Listener (Environment *eenv = 0, unsigned timeout_ms = 1000);

    /**
     * Starts the main server thread -- waits for a connection on (a number of)
     * ports and spawns off the Client corresponding to that port for the
     * accepted socket.
     */
    void start();

    /** Tell all clients and the listener that they should stop ASAP */
    void stop();

    /** Wait for all clients and the listener to finish running */
    int join();

    const CLIENT_VEC& getClients() const;

    /** Tell all the clients that they should stop ASAP */
    void stopClients();

    /** Wait for all clients to finish running */
    void joinClients();

    /** Tell its parent listener that a client has finished */
    void clientFinished(Client *cl);

    /** Did initialisation work? Are we ready to accept connections? */
    bool isReady() {return ready;}

    /** Destructor frees memory and closes all the server sockets */
    ~Listener();

    /** Set whether the hostname should be resolved when a peer connects */
    void setResolveHost(bool _resolvehost) {
        resolvehost = _resolvehost;
    }

private:
    typedef stl::vector<Server*> FACTORY_VEC; ///< Type for list of factories
    static FACTORY_VEC factories;            ///< The factories to start when the main server starts
public:
    /** Add a client handler to the repository (call before init) */
    static bool addServer(Server::CLIENT_FACTORY, Uint16 port);

};

/** A buffered line reader over a socket */
class BufferedLineReader {
protected:
    const Uint32 bsz;  ///< Buffer size
    TCPsocket s;       ///< The socket to read from
    Uint8 *buffer[2];  ///< The buffers
    Uint32 pos;        ///< Current start position
    Uint32 end;        ///< Current filled position
    Uint8 active;      ///< The active buffer

    /**
     * True if we have a full line in the buffer already
     * \returns pointer to the start of the line or NULL if there is no full line
     */
    const char* haveLine() {
        Uint32 oldpos = pos;
        for (; pos < end; ++pos) {
            if (buffer[active][pos] == '\n') {
                pos++;
                return (const char*)buffer[active]+oldpos;
            }
        }
        pos = oldpos;
        return 0;
    }

    /** Fill the buffers, returning a pointer to the first character not yet returned or 0 if we got closed. */
    const char* fillBuffer();

public:
    /** Constructor -- read from \a ss with the specified \a buffer_size */
    BufferedLineReader(TCPsocket ss, Uint32 buffer_size)
        :
    bsz(buffer_size), s(ss), pos(0), end(0), active(0) { //force a switch/fill on first read
        buffer[0] = new Uint8[bsz+1];
        buffer[1] = new Uint8[bsz+1];
        buffer[0][bsz] = 0;
        buffer[1][bsz] = 0;
    }
    /** Return the next line (will end in \n) or the maximum length allowed by \a buffer_size */
    const char* getLine() {
        const char* hl = haveLine();
        return hl ? hl : fillBuffer();
    }
};

/** An in-memory buffer for socket I/O */
template <class UT8 = Uint8, class UT16 = Uint16, class UT32 = Uint32>
struct GeneralBuffer {
    UT8 *bytes;      ///< Start of buffer
    UT8 *offset;     ///< Read/write offset in buffer
    UT8 * const end; ///< End of buffer (for fail-fast errors)

    /** Create a buffer of size \a sz */
    GeneralBuffer (UT32 sz)
        :
    bytes(new UT8[sz]), offset(bytes), end(bytes + sz)
    {
        memset(bytes, 0, sizeof(*bytes)*sz);
    }

    ///\name Boring operations on a buffer
    //@{
    ///Destructor deletes \a bytes
    ~GeneralBuffer() {
        delete[] bytes;
    }
    operator UT8*() {
        return bytes;
    }
    operator char*() {
        return reinterpret_cast<char*>(bytes);
    }
    UT8& operator[](UT32 i) {
        assert(bytes + i <= end);
        return bytes[i];
    }
    GeneralBuffer& operator>>(UT8& b) {
        assert(offset + 1 <= end);
        b = *offset++;
        return *this;
    }
    GeneralBuffer& operator>>(UT16& b) {
        assert(offset + 2 <= end);
        b = static_cast<UT16>(SDLNet_Read16(offset));
        offset += 2;
        return *this;
    }
    GeneralBuffer& operator>>(UT32& b) {
        assert(offset + 4 <= end);
        b = static_cast<UT32>(SDLNet_Read32(offset));
        offset += 4;
        return *this;
    }
    GeneralBuffer& operator<<(UT8 b) {
        assert(offset + 1 <= end);
        *offset++ = b;
        return *this;
    }
    GeneralBuffer& operator<<(UT16 b) {
        assert(offset + 2 <= end);
        SDLNet_Write16(b, offset);
        offset += 2;
        return *this;
    }
    GeneralBuffer& operator<<(UT32 b) {
        assert(offset + 4 <= end);
        SDLNet_Write32(b, offset);
        offset += 4;
        return *this;
    }
    //@}
};

/** Instantiate the default GeneralBuffer using SDL types */
typedef GeneralBuffer<> SDLBuffer;

// Needed by MSVC for empty macro arg.
#define nothing

/**
 * Like SDL_SERVER_DECLARECLIENT(), but gives the class an attribute.
 */
#define SDL_SERVER_DECLARECLIENT_ATTR(_name, _attr) class _attr _name : public Client { \
    protected: /*@{*/ \
    virtual int handle(); \
public: \
    _name (TCPsocket ss, Listener *ccreator); \
    static Client *make(TCPsocket ss, Listener *ccreator); \
    static int &port; \
    static bool added; /*@}*/ \
} \

/**
 * Declare the class interface for a SDL_SERVER Client instantiation.
 */
#define SDL_SERVER_DECLARECLIENT(_name) SDL_SERVER_DECLARECLIENT_ATTR(_name, nothing)

/**
 * Like SDL_SERVER_DEFINECLIENT(), but gives the class an attribute.
 */
#define SDL_SERVER_DEFINECLIENT_ATTR(_name, _port, _attr) SDL_SERVER_DECLARECLIENT_ATTR(_name, _attr); \
    _name::_name (TCPsocket ss, Listener *ccreator) : Client(ss, ccreator) {} \
    int &_name::port = _port; \
    bool _name::added = false; \
    Client *_name::make(TCPsocket ss, Listener *ccreator) {return new _name(ss, ccreator); } \

/**
 * This _defines_ a subclass of Client called \ _name so that you need only
 * define \code int _name::handle() \endcode. Do not do this in a header as we
 * need to allocate "0 bytes" of static address space.
 */
#define SDL_SERVER_DEFINECLIENT(_name, _port) SDL_SERVER_DEFINECLIENT_ATTR(_name, _port, nothing)

/**
 * Add the Client of class interface \a _name to the Listener collection
 */
#define SDL_SERVER_ADDCLIENT(_name, _port); \
    _name::port = _port; \
    _name::added = _name::added || Listener::addServer(&_name::make, _name::port); \

/**
 * Mark the Client (of class interface \a _name) as removed, so that it can be
 * added to a different Listener
 */
#define SDL_SERVER_QUITCLIENT(_name) \
    _name::added = false \

#endif
